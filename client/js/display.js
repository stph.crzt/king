function displayCards() {

  console.log(global.game)
  console.log(global.player)

  razAreas()

  /****************************************************************
  Message
  ****************************************************************/
  if (global.player.messages) {
    sendMessage(global.player.messages[0])
  }

  /****************************************************************
  Display open, stock and shared zones (activate if step 1)
  ****************************************************************/
  createCardsContainer('open', 'common')
  createCardsContainer('stock', 'common')
  createCardsContainer('shared', 'common')
  createCardsContainer('reserve', 'aside')
  const activateCard = global.game.step === 1 && global.game.active.name === global.player.name
  for (let card of global.game.stock) {
    let cardDiv = addCardToSection(card, 'stock')
    if (activateCard) cardDiv.style.cursor = 'pointer'
    else cardDiv.style.cursor = 'not-allowed'
    cardDiv.addEventListener('click', takeCard)
  }
  for (let card of global.game.open) {
    let cardDiv = addCardToSection(card, 'open')
    if (activateCard) cardDiv.style.cursor = 'pointer'
    else cardDiv.style.cursor = 'not-allowed'
    cardDiv.addEventListener('click', takeCard)
  }
  for (let card of global.game.shared) {
    let cardDiv = addCardToSection(card, 'shared')
    cardDiv.style.cursor = 'not-allowed'
  }
  for (let card of global.game.reserve) {
    let cardDiv = addCardToSection(card, 'reserve')
    cardDiv.style.cursor = 'not-allowed'
  }

  /****************************************************************
  Display displayed player and his neighbours
  ****************************************************************/
  const activePlayer = global.game.players.indexOf(global.player)
  let leftPlayer = activePlayer - 1
  if (leftPlayer === -1) leftPlayer = global.game.players.length - 1
  let rightPlayer = activePlayer + 1
  if (rightPlayer === global.game.players.length) rightPlayer = 0
  createPlayer(global.game.players[leftPlayer], 'me')
  createPlayer(global.game.players[activePlayer], 'me')
  createPlayer(global.game.players[rightPlayer], 'me')

  /****************************************************************
  Display other players
  ****************************************************************/
  let i
  for (i = leftPlayer - 1; i >= 0 && i != rightPlayer; i--) {
    createPlayer(global.game.players[i], 'others')
  }
  if (i <= 0 && i != rightPlayer) {
    for (i = global.game.players.length - 1; i > rightPlayer; i--) {
      createPlayer(global.game.players[i], 'others')
    }
  }

  /****************************************************************
  Display commands for steps 0 or 5
  ****************************************************************/
  commandDiv = document.querySelector('#command')
  razCommand(commandDiv)
  displayRefillCommand(global.game.step, commandDiv)
  displayEarningsCommand(global.game.step, commandDiv)
}

/****************************************************************
Reset all areas
****************************************************************/
function razAreas() {
  let areas = document.querySelector('#me')
  while (areas.firstChild) areas.removeChild(areas.firstChild)
  areas = document.querySelector('#others')
  while (areas.firstChild) areas.removeChild(areas.firstChild)
  areas = document.querySelector('#common')
  while (areas.firstChild) areas.removeChild(areas.firstChild)
  areas = document.querySelector('#aside')
  while (areas.firstChild) areas.removeChild(areas.firstChild)
  areas = document.querySelector('#war-info')
  while (areas.firstChild) areas.removeChild(areas.firstChild)
  areas = document.querySelector('#war-support')
  while (areas.firstChild) areas.removeChild(areas.firstChild)
  areas = document.querySelector('#war-send')
  while (areas.firstChild) areas.removeChild(areas.firstChild)
}


/****************************************************************
createCardsContainer
****************************************************************/
function createCardsContainer(id, areaId, name) {

  const area = document.querySelector(`#${areaId}`)

  const mainDiv = document.createElement('div')
  area.appendChild(mainDiv)

  const title = document.createElement('h3')
  mainDiv.appendChild(title)
  if (name) title.innerText = name
  else title.innerText = id

  const cardsDiv = document.createElement('div')
  mainDiv.appendChild(cardsDiv)
  cardsDiv.classList = 'cards'
  cardsDiv.id = id
}

/****************************************************************
createPlayer
****************************************************************/
function createPlayer(player, areaId) {

  //areaId should be me or others
  const area = document.querySelector(`#${areaId}`)

  const playerDiv = document.createElement('div')
  area.appendChild(playerDiv)
  playerDiv.id = player.name
  playerDiv.classList = 'player'

  const title = document.createElement('h2')
  playerDiv.appendChild(title)
  title.innerText += player.name
  if (player.king) title.classList.add('king')

  const production = document.createElement('div')
  playerDiv.appendChild(production)

  const army = document.createElement('div')
  production.appendChild(army)
  army.id = 'army'
  for (let i=0; i < player.army; i++) army.innerText += '⚔'

  const defence = document.createElement('div')
  production.appendChild(defence)
  defence.id = 'defence'
  for (let i=0; i < player.defence; i++) defence.innerText += '♖'

  const gold = document.createElement('div')
  production.appendChild(gold)
  gold.id = 'gold'
  for (let i=0; i < player.goldStock; i++) gold.innerText += '○'
  gold.innerText += ` +${player.gold}`

  const prestige = document.createElement('div')
  production.appendChild(prestige)
  prestige.id = 'prestige'
  for (let i=0; i < player.prestigeStock; i++) prestige.innerText += '☆'
  prestige.innerText += ` +${player.prestige}`

  const resources = document.createElement('div')
  production.appendChild(resources)
  resources.id = 'resources'
  resources.innerText = `▢${player.resources.sort().map(v => v.substr(0,3))}`

  const hand = `${player.name}-hand`
  createCardsContainer(hand, player.name, '🂠🂠🂠')
  for (let card of player.hand) {
    let cardDiv = addCardToSection(card, hand)
    if (player == global.player) {
      if (global.game.step === 2) {
        cardDiv.style.cursor = 'grabbing'
        cardDiv.addEventListener('click', playCard)
      }
    }
  }

  const boardDiv = document.createElement('div')
  playerDiv.appendChild(boardDiv)
  const board = `${player.name}-board`
  boardDiv.id = board
  boardDiv.classList = 'board'

  //TODO factorise this code
  const left = `${player.name}-left`
  createCardsContainer(left, board, '🠔')
  for (let card of player.left) {
    let cardDiv = addCardToSection(card, left)
    cardDiv.style.cursor = 'not-allowed'
    cardDiv.area = left
    if (player == global.player) {
      if (global.game.step === 3) {
        cardDiv.style.cursor = 'pointer'
        cardDiv.addEventListener('click', activateCard)
      }
      else if (global.game.step === 4) {
        cardDiv.style.cursor = 'pointer'
        cardDiv.addEventListener('click', returnCard)
      }
    }
    else if (global.game.attack && global.game.attack.resolution > 0 && player.name === global.game.attack.defender.player.name) {
      cardDiv.style.cursor = 'pointer'
      cardDiv.defender = player.name
      cardDiv.addEventListener('click', destroyCard)
    }
  }
  const center = `${player.name}-center`
  createCardsContainer(center, board, '⌂')
  for (let card of player.center) {
    let cardDiv = addCardToSection(card, center)
    cardDiv.style.cursor = 'not-allowed'
    cardDiv.area = center
    if (player == global.player) {
      if (global.game.step === 3) {
        cardDiv.style.cursor = 'pointer'
        cardDiv.addEventListener('click', activateCard)
      }
      else if (global.game.step === 4) {
        cardDiv.style.cursor = 'pointer'
        cardDiv.addEventListener('click', returnCard)
      }
    }
    else if (global.game.attack && global.game.attack.resolution > 0 && player.name === global.game.attack.defender.player.name) {
      cardDiv.style.cursor = 'pointer'
      cardDiv.defender = player.name
      cardDiv.addEventListener('click', destroyCard)
    }
  }
  const right = `${player.name}-right`
  createCardsContainer(right, board, '➞')
  for (let card of player.right) {
    let cardDiv = addCardToSection(card, right)
    cardDiv.style.cursor = 'not-allowed'
    cardDiv.area = right
    if (player == global.player) {
      if (global.game.step === 3) {
        cardDiv.style.cursor = 'pointer'
        cardDiv.addEventListener('click', activateCard)
      }
      else if (global.game.step === 4) {
        cardDiv.style.cursor = 'pointer'
        cardDiv.addEventListener('click', returnCard)
      }
    }
    else if (global.game.attack && global.game.attack.resolution > 0 && player.name === global.game.attack.defender.player.name) {
      cardDiv.style.cursor = 'pointer'
      cardDiv.defender = player.name
      cardDiv.addEventListener('click', destroyCard)
    }
  }

  const footer = document.createElement('div')
  playerDiv.appendChild(footer)
  footer.classList.add('player-footer')

  if (global.game.step === 3) {
    const pass = document.createElement('span')
    footer.appendChild(pass)
    pass.id = 'pass'
    if (player == global.player) {
      pass.addEventListener('click', passing)
      pass.innerText = 'Pass'
    }
    if (player.passing) {
      pass.innerText = 'Passing...'
    }
  }

  setAttackInteraction(player, playerDiv)

}

/****************************************************************
addCardToSection
****************************************************************/
function addCardToSection(card, areaName) {
  const areaSection = document.querySelector(`#${areaName}`)
  const newCard = document.createElement("div")
  areaSection.appendChild(newCard)
  newCard.id = card.id
  newCard.name = card.name
  newCard.classList.add('card')
  newCard.classList.add(card.type)

  const isActivePlayerCard = areaSection.closest('.player') && areaSection.closest('.player').id === global.player.name
  if (card.visible || isActivePlayerCard) {
    // Name
    const name = document.createElement("div")
    newCard.appendChild(name)
    name.innerText = card.name
    name.classList.add('name')
    // Cost
    const cost = document.createElement("div")
    newCard.appendChild(cost)
    cost.classList.add('cost')
    cost.innerText = 'cost:'
    if (card.cost) {
      cost.innerText += card.cost.sort().map(v => v.substr(0,3))
    }
    else {
      cost.innerText += '-'
    }
    // Benefits
    const bene = document.createElement("div")
    bene.classList.add('benefits')
    newCard.appendChild(bene)
    if (card.resources) bene.innerText += `${card.resources.map(v => `▢${v.substr(0,3)}`)} `
    if (card.army) bene.innerText += `⚔${card.army} `
    if (card.defence) bene.innerText += `♖${card.defence} `
    if (card.gold) bene.innerText += `○${card.gold} `
    if (card.prestige) bene.innerText += `☆${card.prestige} `
  }
  if (!card.visible && isActivePlayerCard) {
    newCard.classList.add('not-visible-card')
  }

  return newCard
}
