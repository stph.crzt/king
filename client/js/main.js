const global = {
  game: null,
  player: null
}

initGame()
window.setInterval(initGame, 300000)
window.addEventListener('focus', initGame)
document.querySelector('#load').addEventListener('click', initGame)

async function initGame() {
  await getGame()
  displayGame()
}

async function getGame() {
  const response = await fetch('/init')
  global.game = await response.json()
}

function displayGame() {
  displayOrder(document.querySelector('#order'))
  document.querySelector('#active-player').innerText = global.game.active.name
  document.querySelector('#turn').innerText = global.game.turn
  document.querySelector('#step').innerText = global.game.step
  document.querySelector('#game').innerText = global.game.name
  connectPlayer()
  displayCards()
}

function displayOrder(orderDiv) {
  orderDiv.innerText = null
  for (p of global.game.order) {
    const span = document.createElement("span")
    orderDiv.appendChild(span)
    span.innerText = p.name
    if (p.name === global.game.active.name) span.classList.add('active')
  }
}

function connectPlayer() {
    let i = 0
    connected = false
    const currentPlayer = document.querySelector('#connected-player')
    const currentPass = document.querySelector('#pass')
    document.querySelector('#connected').innerText = 'Not Connected'

    while (i < global.game.players.length && !connected) {
      let p = global.game.players[i]
      if (currentPlayer.value === p.name && currentPass.value === p.password) {
        global.player = p
        document.querySelector('#connected').innerText = 'Connected'
        connected = true
      }
      i++
    }
}
