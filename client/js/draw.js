function takeCard() {
  if (global.game.active.name === global.player.name) {
    if (global.player.hand.length < 3) {
      let area
      if (this.closest('#stock')) {
        area = 0
      }
      else if (this.closest('#open')) {
        area = 1
      }
      const order = JSON.parse(`{
        "order": "takeCard",
        "card": "${this.id}",
        "player": "${global.player.name}",
        "area": ${area}
      }`)
      sendOrder(order)
    }
  }
}
