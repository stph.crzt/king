function calculateEarnings() {
  const order = JSON.parse(`{
    "order": "earnings"
  }`)
  sendOrder(order)
}

function displayEarningsCommand(step, commandDiv) {
  if (step === 5) {
    const span = document.createElement('span')
    commandDiv.appendChild(span)
    span.innerText = 'Calculte earnings'
    span.addEventListener('click', calculateEarnings)
  }
}
