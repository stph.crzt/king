function activateCard() {
  if (global.game.active.name === global.player.name) {
    const order = JSON.parse(`{
      "order": "activateCard",
      "card": "${this.id}",
      "player": "${global.player.name}",
      "area": "${this.area}"
    }`)
    sendOrder(order)
  }
}
