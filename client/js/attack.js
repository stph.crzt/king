function attack() {
  if (global.game.step === 6) {
    if (global.game.active.name === global.player.name) {
      if (confirm(`Attack ${this.target} with army = ${this.value} ?`)) {
        const order = JSON.parse(`{
          "order": "attack",
          "player": "${global.player.name}",
          "target": "${this.target}",
          "army": ${this.value}
        }`)
        sendOrder(order)
        document.querySelector('#war-send').innerText = null
      }
    }
  }
}

function doNotAttack() {
  if (global.game.step === 6) {
    if (global.game.active.name === global.player.name) {
      if (confirm(`Peace and love ?`)) {
        const order = JSON.parse(`{
          "order": "doNotAttack",
          "player": "${global.player.name}"
        }`)
        sendOrder(order)
        document.querySelector('#war-send').innerText = null
      }
    }
  }
}

function defend() {
  if (global.game.step === 6) {
    if (global.game.active.name === global.player.name) {
      if (confirm(`Defend with army = ${this.value}  ?`)) {
        document.querySelector('#war-send').innerText = null
        const order = JSON.parse(`{
          "order": "defend",
          "player": "${global.player.name}",
          "army": ${this.value}
        }`)
        sendOrder(order)
      }
    }
  }
}

function support() {
  if (global.game.step === 6) {
    if (global.game.active.name === global.player.name) {
      if (confirm(`Participate in war ${global.game.attack.attacker.player.name} against ${global.game.attack.defender.player.name} with action = ${this.support} and army = ${this.value}`)) {
        document.querySelector('#war-send').innerText = null
        document.querySelector('#war-support').innerText = null
        const order = JSON.parse(`{
          "order": "support",
          "player": "${global.player.name}",
          "support": "${this.support}",
          "army": ${this.value}
        }`)
        sendOrder(order)
      }
    }
  }
}

function supportPass() {
  if (global.game.step === 6) {
    if (global.game.active.name === global.player.name) {
      if (confirm(`Do not participate in this uggly war ?`)) {
        document.querySelector('#war-support').innerText = null
        this.disabled = true
        const order = JSON.parse(`{
          "order": "support",
          "player": "${global.player.name}",
          "support": "pass"
        }`)
        sendOrder(order)
      }
    }
  }
}

function endAttack() {
  if (global.game.step === 6) {
    if (global.game.active.name === global.player.name) {
      this.disabled = true
      const order = JSON.parse(`{
        "order": "endAttack"
      }`)
      sendOrder(order)
    }
  }
}

function destroyCard() {
  const order = JSON.parse(`{
    "order": "destroyCard",
    "card": "${this.id}",
    "attacker": "${global.player.name}",
    "defender": "${this.defender}",
    "area": "${this.area}"
  }`)
  sendOrder(order)
}


function setAttackInteraction(player, playerDiv) {
  const isDisplayedPlayer = player.name === global.player.name
  //console.log(isDisplayedPlayer)
  const isActivePlayer = global.game.active.name === global.player.name
  //console.log(isActivePlayer)
  const isNewAttack = global.game.attack === null
  //console.log(isNewAttack)
  const isNotDefender = global.game.attack && global.game.attack.defender.player.name != global.player.name
  const hasNotDefended = global.game.attack && global.game.attack.defender.army == null
  if (global.game.step === 6) {
    if (isDisplayedPlayer) {
      if (isActivePlayer) {
        // Declares new attack
        if (isNewAttack) {
          const warSend = document.querySelector('#war #war-support')
          const button = document.createElement("button")
          warSend.appendChild(button)
          button.innerText = 'Do not attack'
          button.target = null
          button.addEventListener('click', doNotAttack)
        }
        // Declares support
        else if (isNotDefender && hasNotDefended) {
          displayAttackState()
          displaySupportChoice()
        }
        // Declares defence
        else if (hasNotDefended) {
          displayAttackState()
          displayNoDefence()
          sendArmy()

        }
        // Attack resolution
        else {
          displayAttackState()
          displayAttackResolution()
        }
      }
      // Unactive player
      else {
        displayAttackState()
      }
    }
    else { //isNotDisplayedPlayer
      if (isNewAttack && isActivePlayer) {
        playerDiv.style.cursor = 'crosshair'
        playerDiv.addEventListener('click', chooseTarget)
      }
    }
  }
}

function chooseTarget() {
  for (title of document.querySelectorAll(`div.player`)) title.classList.remove('attacked')
  this.classList.add('attacked')
  targetName = this.querySelector('h2').innerText
  sendArmy(targetName)
}

function displayAttackState() {
  const attack = global.game.attack
  if (attack) {
    const infoDiv = document.querySelector('#war #war-info')
    infoDiv.innerText = null
    const divAttacktState = document.createElement('div')
    infoDiv.appendChild(divAttacktState)
    divAttacktState.innerText = `${attack.attacker.player.name} attacks ${attack.defender.player.name} [army : ${attack.attacker.army}]`
    for (s of attack.attackerSupport) {
      divAttacktState.innerText = `${divAttacktState.innerText}
        ${s.player.name} supports attack [army : ${s.army}]`
    }
    for (s of attack.defenderSupport) {
      divAttacktState.innerText = `${divAttacktState.innerText}
        ${s.player.name} supports defence [army : ${s.army}]`
    }
    if (attack.resolution != null) {
      divAttacktState.innerText = `${divAttacktState.innerText}
        Résolution : ${attack.resolution}`
    }
  }
}

function displaySupportChoice () {
  const supportDiv = document.querySelector('#war #war-support')
  supportDiv.innerText = null
  const support = radioDiv('Support', ['Attack', 'Defence', 'Pass'])
  supportDiv.appendChild(support)
  support.addEventListener('input', chooseSupport)
}

function displayNoDefence() {
  const defenceDiv = document.querySelector('#war #war-support')
  defenceDiv.innerText = null
  const button = document.createElement('button')
  defenceDiv.appendChild(button)
  button.innerText = 'Do not defend'
  button.value = 0
  button.addEventListener('click', defend)

}


function radioDiv(name, values) {
  const mainDiv = document.createElement('div')
  const mainDivTitle = document.createElement('div')
  mainDiv.appendChild(mainDivTitle)
  mainDivTitle.innerText = name
  mainDiv.classList.add('radio')
  for (v of values) {
    const id = v.toLowerCase()
    const radio = document.createElement('div')
    const input = document.createElement('input')
    input.type = 'radio'
    input.name = name.toLowerCase()
    input.value = v.toLowerCase()
    input.id = id
    const label = document.createElement('label')
    label.for = id
    label.innerText = `${v}`
    radio.appendChild(input)
    radio.appendChild(label)
    mainDiv.appendChild(radio)
  }
  return mainDiv
}

function chooseSupport() {
  const support = document.querySelector('input[name="support"]:checked').value
  if (support != 'pass') {
    const sendArmyDiv = document.querySelector('#sendArmy')
    if (sendArmyDiv) sendArmyDiv.innerText = null
    sendArmy(null, support)
  }
  else {
    supportPass()
  }
}

function sendArmy(target = null, supportType = null) {
  const playerCanSendOneArmy = (global.player.army > 0 && global.player.goldStock > 0) || global.player.goldStock > 1

  if (playerCanSendOneArmy) {
    const warSend = document.querySelector('#war #war-send')
    warSend.innerText = null
    const sendText = document.createElement('div')
    sendText.innerText = `Send army`
    warSend.appendChild(sendText)

    const send = document.createElement('select')
    warSend.appendChild(send)
    send.appendChild(document.createElement("option"))
    /**
    //defending
    if (target === null && supportType === null) {
      zero = document.createElement("option")
      zero.value = 0
      zero.text = '0 (cost : 0)'
      send.add(zero)
    }
    **/
    send.id = 'sendArmy'
    let maxCostOneGoldArmy = Math.min(global.player.army, global.player.goldStock)
    for (let i = 1; i <= maxCostOneGoldArmy; i++) {
      const opt = document.createElement('option')
      opt.value = i
      opt.text = `${i} (cost : ${i})`
      send.add(opt)
    }
    for (let i = 1; i <= Math.floor((global.player.goldStock - global.player.army) / 2); i++) {
      const opt = document.createElement("option")
      opt.value = global.player.army + i
      opt.text = `${global.player.army + i} (cost : ${global.player.army + 2 * i})`
      send.add(opt)
    }
    // supporting
    if (supportType) {
      send.support = supportType
      send.addEventListener('input', support)
    }
    else {
      // defending
      if (global.game.attack) {
        send.addEventListener('input', defend)
      }
      // attacking
      else {
        send.target = target
        send.addEventListener('input', attack)
      }
    }
  }
}

function displayAttackResolution() {
  const warSend = document.querySelector('#war #war-send')
  warSend.innerText = null
  // OLD condition : if (global.game.attack.resolution <= 0) {
  const button = document.createElement("button")
  warSend.appendChild(button)
  button.innerText = 'End attack'
  button.addEventListener('click', endAttack)

}
