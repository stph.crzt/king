function playCard() {
    document.querySelectorAll('section#me div.card').forEach(div => div.classList.remove("selected-card"))
    this.classList.add("selected-card")

    let areas = []
    areas.push(document.querySelector(`#${global.player.name}-left`))
    areas.push(document.querySelector(`#${global.player.name}-center`))
    areas.push(document.querySelector(`#${global.player.name}-right`))
    areas.forEach((area) => {
      area.style.cursor = 'grab'
      area.addEventListener('click', dropCard)
      area.card = this
    });
}

function dropCard() {
  const order = JSON.parse(`{
    "order": "playCard",
    "card": "${this.card.id}",
    "player": "${global.player.name}",
    "area": "${this.id}"
  }`)
  sendOrder(order)
}
