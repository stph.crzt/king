function refill() {
  const order = JSON.parse(`{
    "order": "refill"
  }`)
  sendOrder(order)
}

function razCommand(commandDiv) {
  while (commandDiv.firstChild) commandDiv.removeChild(commandDiv.firstChild)
}

function displayRefillCommand(step, commandDiv) {
  if (step === 0) {
    const span = document.createElement('span')
    commandDiv.appendChild(span)
    span.innerText = 'Refill stock with new cards'
    span.addEventListener('click', refill)
  }
}
