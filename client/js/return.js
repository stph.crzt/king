function returnCard() {
  const order = JSON.parse(`{
    "order": "returnCard",
    "card": "${this.id}",
    "player": "${global.player.name}",
    "area": "${this.area}"
  }`)
  sendOrder(order)
}
