async function sendOrder(order) {
  console.log(order)
  let response = await fetch('/order', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(order)
  })
  global.game = await response.json()
  displayGame()
}

function passing() {
  if (global.game.active.name === global.player.name) {
    const order = JSON.parse(`{
      "order": "pass",
      "player": "${global.player.name}"
    }`)
    sendOrder(order)
  }
}
