#!/bin/bash

echo Transpiling...
tsc --project server/ts

# common js
cp server/js/Area.js client/server_js

node server/js/server.js --port 8082
