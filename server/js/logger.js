"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const winston_1 = require("winston");
const consoleLog = new winston_1.transports.Console({ format: winston_1.format.simple() });
const errorLog = new winston_1.transports.File({ filename: 'log/error.log', level: 'error' });
const miscLog = new winston_1.transports.File({ filename: 'log/misc.log' });
exports.logger = winston_1.createLogger({
    format: winston_1.format.combine(winston_1.format.timestamp(), winston_1.format.json()),
    transports: [consoleLog, errorLog, miscLog]
});
