"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Engagement {
    constructor(player, army = null) {
        this.player = player;
        this.army = army;
    }
    addArmy(army) {
        this.army += army;
    }
}
class Attack {
    constructor(order, attacker, attackerArmy, defender) {
        this.attackerSupport = [];
        this.defenderSupport = [];
        if (this.spendArmyCost(attacker, attackerArmy)) {
            this.attacker = new Engagement(attacker, attackerArmy);
            this.defender = new Engagement(defender);
            this.undefinedPlayers = [].concat(order);
            this.undefinedPlayers.splice(this.undefinedPlayers.indexOf(attacker), 1);
            this.undefinedPlayers.splice(this.undefinedPlayers.indexOf(defender), 1);
        }
        else
            throw 'Fatal error';
    }
    spendArmyCost(player, armySent) {
        let cost;
        cost = Math.min(player.getArmy(), armySent);
        console.log(cost);
        if (armySent > player.getArmy())
            cost += (armySent - player.getArmy()) * 2;
        console.log(cost);
        console.log(player.getGoldStock());
        if (player.getGoldStock() >= cost) {
            player.spendGold(cost);
            return true;
        }
        else
            return false;
    }
    support(player, supportType, army = 0) {
        if (this.spendArmyCost(player, army)) {
            if (this.undefinedPlayers[0] === player) {
                if (supportType === 'attack') {
                    this.attackerSupport.push(new Engagement(player, army));
                    this.undefinedPlayers.shift();
                    return true;
                }
                else if (supportType === 'defence') {
                    this.defenderSupport.push(new Engagement(player, army));
                    this.undefinedPlayers.shift();
                    return true;
                }
                else if (supportType === 'pass') {
                    this.undefinedPlayers.shift();
                    return true;
                }
            }
        }
        else
            throw 'Fatal error';
    }
    defend(player, army) {
        if (this.spendArmyCost(player, army)) {
            if (this.defender.player === player) {
                this.defender.army = army;
                this.resolution = this.calculateResolution();
                return true;
            }
        }
        else
            throw 'Fatal error';
    }
    calculateResolution() {
        let resolution = this.attacker.army - this.defender.army - this.defender.player.getDefence();
        if (this.attackerSupport.length > 0)
            resolution += this.attackerSupport.map(v => v.army).reduce((total, v) => total + v);
        if (this.defenderSupport.length > 0)
            resolution -= this.defenderSupport.map(v => v.army).reduce((total, v) => total + v);
        return resolution;
    }
    nextPlayer() {
        if (this.undefinedPlayers.length > 0)
            return this.undefinedPlayers[0];
        else if (this.defender.army == null)
            return this.defender.player;
        else
            return null;
    }
    destroyOneCard(attacker, defender, card, area) {
        if (attacker === this.attacker.player && defender === this.defender.player && this.resolution > 0) {
            this.defender.player.destroyCard(card, area);
            this.resolution--;
            return true;
        }
    }
}
exports.Attack = Attack;
