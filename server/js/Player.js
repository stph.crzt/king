"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Area_1 = require("./Area");
class Player {
    constructor(player) {
        this.hand = [];
        this.center = [];
        this.left = [];
        this.right = [];
        this.resources = [];
        this.army = 0;
        this.defence = 0;
        this.gold = 0;
        this.goldStock = 0;
        this.prestige = 0;
        this.prestigeStock = 0;
        this.king = false;
        this.passing = false; //if true player is passing for this step
        this.messages = [];
        Object.assign(this, player);
    }
    getName() {
        return this.name;
    }
    getPrestige() {
        return this.prestige;
    }
    getGoldStock() {
        return this.goldStock;
    }
    getDefence() {
        return this.defence;
    }
    getArmy() {
        return this.army;
    }
    getPrestigeWithNeigbours() {
        return this.prestige + (this.leftPlayer.prestige + this.rightPlayer.prestige) / 1000;
    }
    getCards() {
        return this.hand.concat(this.left).concat(this.center).concat(this.right);
    }
    maxPrestigeBonus() {
        if (this.prestige < 3)
            this.goldStock += this.prestige;
        else
            this.goldStock += 3;
        console.log(`${this.getName()} earns prestige bonus`);
    }
    makeKing() {
        this.king = true;
    }
    unmakeKing() {
        this.king = false;
    }
    isKing() {
        return this.king;
    }
    earGold(gold) {
        this.goldStock += gold;
    }
    spendGold(gold) {
        this.goldStock -= gold;
    }
    setNeighbours(leftPlayer, rightPlayer) {
        this.leftPlayer = leftPlayer;
        this.rightPlayer = rightPlayer;
    }
    getNeighbour(area) {
        if (area === Area_1.Area.LEFT) {
            return this.leftPlayer;
        }
        else if (area === Area_1.Area.RIGHT) {
            return this.rightPlayer;
        }
        else {
            return null;
        }
    }
    addMessage(message) {
        this.messages.unshift(message);
        if (this.messages.length > Player.MAX_MESSAGES)
            this.messages.pop();
    }
    noCardToReturn() {
        return this.getLeft().length <= Area_1.AreaMaxCards.LEFT
            && this.getRight().length <= Area_1.AreaMaxCards.RIGHT
            && this.getCenter().length <= Area_1.AreaMaxCards.CENTER;
    }
    getCardById(cardId, area = null) {
        if (area === Area_1.Area.HAND) {
            for (const card of this.hand) {
                if (card.getId() === cardId)
                    return card;
            }
        }
        if (area === Area_1.Area.LEFT) {
            for (const card of this.left) {
                if (card.getId() === cardId)
                    return card;
            }
        }
        if (area === Area_1.Area.CENTER) {
            for (const card of this.center) {
                if (card.getId() === cardId)
                    return card;
            }
        }
        if (area === Area_1.Area.RIGHT) {
            for (const card of this.right) {
                if (card.getId() === cardId)
                    return card;
            }
        }
        // search any area
        if (area === null) {
            for (const card of this.getCards()) {
                if (card.getId() === cardId)
                    return card;
            }
        }
        // else
        return null;
    }
    /** In order to be activable the cost of the card must be already available for the player **/
    isActivable(card) {
        let available = this.resources.map((v) => v);
        if (card.cost) {
            for (const resource of card.cost) {
                const i = available.indexOf(resource);
                if (i >= 0) {
                    available.splice(i, 1);
                }
                else {
                    return false;
                }
            }
        }
        return true;
    }
    getHand() {
        return this.hand;
    }
    getCenter() {
        return this.center;
    }
    getLeft() {
        return this.left;
    }
    getRight() {
        return this.right;
    }
    addToHand(card) {
        this.hand.push(card);
    }
    takeFromHand(card) {
        this.hand.splice(this.hand.indexOf(card), 1);
    }
    addProduction(card) {
        if (card.army)
            this.army += card.army;
        if (card.defence)
            this.defence += card.defence;
        if (card.prestige)
            this.prestige += card.prestige;
        if (card.gold)
            this.gold += card.gold;
        if (card.resources)
            this.resources = this.resources.concat(card.getResources());
    }
    removeProduction(card) {
        if (card.isVisible()) {
            if (card.army)
                this.army -= card.army;
            if (card.defence)
                this.defence -= card.defence;
            if (card.prestige)
                this.prestige -= card.prestige;
            if (card.gold)
                this.gold -= card.gold;
            if (card.resources)
                for (let r of card.resources)
                    this.resources.splice(this.resources.indexOf(r), 1);
        }
    }
    playCard(card, area) {
        if (this.hand.includes(card)) {
            //testing possibility
            if (area === Area_1.Area.CENTER && this.center.length < Area_1.AreaMaxCards.CENTER + 1) {
                this.takeFromHand(card);
                this.center.push(card);
                return true;
            }
            else if (area === Area_1.Area.LEFT && this.left.length < Area_1.AreaMaxCards.LEFT + 1) {
                this.takeFromHand(card);
                this.left.push(card);
                return true;
            }
            else if (area === Area_1.Area.RIGHT && this.right.length < Area_1.AreaMaxCards.RIGHT + 1) {
                this.takeFromHand(card);
                this.right.push(card);
                return true;
            }
            else {
                // error
                return false;
            }
        }
    }
    returnCard(card, area) {
        if (area === Area_1.Area.CENTER && this.center.length > Area_1.AreaMaxCards.CENTER && this.center.includes(card)) {
            this.center.splice(this.center.indexOf(card), 1);
            return true;
        }
        else if (area === Area_1.Area.LEFT && this.left.length > Area_1.AreaMaxCards.LEFT && this.left.includes(card)) {
            this.left.splice(this.left.indexOf(card), 1);
            return true;
        }
        else if (area === Area_1.Area.RIGHT && this.right.length > Area_1.AreaMaxCards.RIGHT && this.right.includes(card)) {
            this.right.splice(this.right.indexOf(card), 1);
            return true;
        }
    }
    destroyCard(card, area) {
        if (area === Area_1.Area.CENTER && this.center.includes(card)) {
            this.center.splice(this.center.indexOf(card), 1);
            return true;
        }
        else if (area === Area_1.Area.LEFT && this.left.includes(card)) {
            this.left.splice(this.left.indexOf(card), 1);
            return true;
        }
        else if (area === Area_1.Area.RIGHT && this.right.includes(card)) {
            this.right.splice(this.right.indexOf(card), 1);
            return true;
        }
    }
    earnings() {
        this.goldStock += this.gold;
        this.prestigeStock += this.prestige;
    }
    setPassing() {
        this.passing = true;
    }
    unsetPassing() {
        this.passing = false;
    }
    isPassing() {
        return this.passing;
    }
    cardsInHand() {
        return this.hand.length;
    }
}
exports.Player = Player;
Player.MAX_MESSAGES = 10;
