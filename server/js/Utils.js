"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Utils {
    static shuffle(array) {
        const rList = array.map((v) => [Math.random(), v]);
        return [].concat(rList.sort().map((v) => v[1]));
    }
}
exports.Utils = Utils;
