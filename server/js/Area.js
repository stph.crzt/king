"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Area;
(function (Area) {
    Area[Area["STOCK"] = 0] = "STOCK";
    Area[Area["OPEN"] = 1] = "OPEN";
    Area[Area["SHARED"] = 2] = "SHARED";
    Area[Area["HAND"] = 3] = "HAND";
    Area[Area["CENTER"] = 4] = "CENTER";
    Area[Area["LEFT"] = 5] = "LEFT";
    Area[Area["RIGHT"] = 6] = "RIGHT";
})(Area = exports.Area || (exports.Area = {}));
var AreaMaxCards;
(function (AreaMaxCards) {
    AreaMaxCards[AreaMaxCards["HAND"] = 3] = "HAND";
    AreaMaxCards[AreaMaxCards["CENTER"] = 3] = "CENTER";
    AreaMaxCards[AreaMaxCards["LEFT"] = 1] = "LEFT";
    AreaMaxCards[AreaMaxCards["RIGHT"] = 1] = "RIGHT";
})(AreaMaxCards = exports.AreaMaxCards || (exports.AreaMaxCards = {}));
