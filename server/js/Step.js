"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Step;
(function (Step) {
    Step[Step["INIT"] = 0] = "INIT";
    Step[Step["DRAW"] = 1] = "DRAW";
    Step[Step["PLAY"] = 2] = "PLAY";
    Step[Step["ACTIVATE"] = 3] = "ACTIVATE";
    Step[Step["RETURN"] = 4] = "RETURN";
    Step[Step["EARNINGS"] = 5] = "EARNINGS";
    Step[Step["ATTACK"] = 6] = "ATTACK";
})(Step = exports.Step || (exports.Step = {}));
