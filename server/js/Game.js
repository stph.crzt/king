"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Utils_1 = require("./Utils");
const Player_1 = require("./Player");
const Card_1 = require("./Card");
const Area_1 = require("./Area");
const Step_1 = require("./Step");
const Msg_1 = require("./Msg");
const Attack_1 = require("./Attack");
// fs
const fs_extra_1 = __importDefault(require("fs-extra"));
class Game {
    constructor() {
        this.players = [];
        this.reserve = [];
        this.stock = [];
        this.open = [];
        this.shared = [];
        this.turn = 1;
        this.step = Step_1.Step.INIT;
        this.order = [];
        this.attack = null;
    }
    async init() {
        //Init cards
        const gameJson = await fs_extra_1.default.readJson(`${Game.JSONPATH}/games/basic.json`);
        this.name = gameJson.name;
        for (const set of gameJson.sets) {
            const setJson = await fs_extra_1.default.readJson(`${Game.JSONPATH}/sets/${set}.json`);
            for (const card of setJson.cards) {
                const cardJson = await fs_extra_1.default.readJson(`${Game.JSONPATH}/cards/${card}.json`);
                this.reserve.push(new Card_1.Card(cardJson));
            }
        }
        // Init players
        for (const player of gameJson.players) {
            const playerJson = await fs_extra_1.default.readJson(`${Game.JSONPATH}/players/${player}.json`);
            this.players.push(new Player_1.Player(playerJson));
        }
        this.players = Utils_1.Utils.shuffle(this.players);
        let leftPlayerIndice = this.players.length - 1; // left player of first player is last player
        let rightPlayerIndice = 1; // right player of first player (i.e. player n°0) is player n°1
        for (let i = 0; i < this.players.length; i++) {
            this.players[i].setNeighbours(this.players[leftPlayerIndice], this.players[rightPlayerIndice]);
            leftPlayerIndice++;
            rightPlayerIndice++;
            if (rightPlayerIndice === this.players.length) {
                rightPlayerIndice = 0;
            }
            if (leftPlayerIndice === this.players.length) {
                leftPlayerIndice = 0;
            }
        }
        this.active = this.players[0];
        const i = Math.floor((this.players.length) * Math.random());
        this.makeNewKing(this.players[i], true);
        this.test(Step_1.Step.ACTIVATE);
    }
    /***************************************************************
    Step 0 : Init
    ***************************************************************/
    refill() {
        //this.stock = Utils.shuffle(this.stock)
        //this.nextStep()
        if (this.step === Step_1.Step.INIT) {
            this.addCardFromReserveToStock(this.getPlayersNumber() * Game.PARAM_NB_CARDS_DRAWN_BY_TURN);
            if (this.turn === 1)
                this.addCardFromReserveToStock(this.getPlayersNumber() * Game.PARAM_NB_CARDS_DRAWN_BY_TURN);
            this.nextStep();
        }
    }
    addCardFromReserveToStock(n) {
        // TODO management if no more cards in reserve
        let c;
        for (let i = 0; i < n; i++) {
            c = this.reserve.shift();
            c.setInvisible();
            this.stock.push(c);
        }
        Utils_1.Utils.shuffle(this.stock);
    }
    /***************************************************************
    Step 1 : Draw
    ***************************************************************/
    takeCardFromStock(playerName, cardName) {
        this.takeCardFrom(playerName, cardName, Area_1.Area.STOCK);
    }
    takeCardFromOpen(playerName, cardName) {
        this.takeCardFrom(playerName, cardName, Area_1.Area.OPEN);
    }
    takeCardFrom(playerName, cardId, area) {
        const player = this.getPlayerByName(playerName);
        if (this.step === Step_1.Step.DRAW) {
            if (player === this.active) {
                const card = this.getCardById(cardId, area);
                if (player.cardsInHand() < 3 && card) {
                    if (area === Area_1.Area.STOCK) {
                        this.takeFromStock(card);
                    }
                    else if (area === Area_1.Area.OPEN) {
                        this.takeFromOpen(card);
                        card.setInvisible();
                    }
                    player.addToHand(card);
                    this.nextPlayer();
                    this.sendMessage(`${playerName} takes ${card.getName()} from ${area}`, player);
                    if (this.totalCardsInPlayersHand() === this.players.length * 3) {
                        this.nextStep();
                    }
                }
                else
                    player.addMessage(Msg_1.Msg.CARD_TOOMANY);
            }
            else
                player.addMessage(Msg_1.Msg.PLAYER_WRONG);
        }
        else
            player.addMessage(Msg_1.Msg.STEP_WRONG);
    }
    takeFromStock(card) {
        return this.stock.splice(this.stock.indexOf(card), 1)[0];
    }
    takeFromOpen(card) {
        return this.open.splice(this.open.indexOf(card), 1)[0];
    }
    /***************************************************************
    Step 2 : Play
    ***************************************************************/
    playCardFromHand(playerName, cardId, areaName) {
        const player = this.getPlayerByName(playerName);
        if (this.step === Step_1.Step.PLAY) {
            if (player === this.active) {
                const area = this.getArea(player, areaName);
                const card = player.getCardById(cardId, Area_1.Area.HAND);
                if (player.playCard(card, area)) {
                    this.nextPlayer();
                    if (this.totalCardsInPlayersHand() === 0) {
                        this.nextStep();
                        player.addMessage(`${playerName} plays ${card.getName()} to ${areaName}`);
                    }
                }
                else
                    player.addMessage(Msg_1.Msg.CARD_CANTPLAY);
            }
            else
                player.addMessage(Msg_1.Msg.PLAYER_WRONG);
        }
        else
            player.addMessage(Msg_1.Msg.STEP_WRONG);
    }
    /***************************************************************
    Step 3 : Activate
    ***************************************************************/
    activateCard(playerName, cardId, areaName) {
        const player = this.getPlayerByName(playerName);
        if (this.step === Step_1.Step.ACTIVATE) {
            if (player === this.active) {
                const area = this.getArea(player, areaName);
                const card = player.getCardById(cardId, area);
                // Card already activated ?
                if (card.isVisible()) {
                    player.addMessage(Msg_1.Msg.CARD_ALREADYACTIVE);
                    return false;
                }
                // Enough resources to activate ?
                else if (player.isActivable(card)) {
                    card.setVisible();
                    player.addProduction(card);
                    if (area === Area_1.Area.LEFT || area === Area_1.Area.RIGHT) {
                        player.getNeighbour(area).addProduction(card);
                    }
                    this.nextPlayer();
                    player.addMessage(Msg_1.Msg.DONE);
                }
                else
                    player.addMessage(Msg_1.Msg.CARD_NOTACTIVABLE);
            }
            else
                player.addMessage(Msg_1.Msg.PLAYER_WRONG);
        }
        else
            player.addMessage(Msg_1.Msg.STEP_WRONG);
    }
    pass(playerName) {
        const player = this.getPlayerByName(playerName);
        if (this.step === Step_1.Step.ACTIVATE) {
            if (player === this.active) {
                player.setPassing();
                this.nextPlayer();
                player.addMessage(Msg_1.Msg.PASSING);
            }
            else
                player.addMessage(Msg_1.Msg.PLAYER_WRONG);
        }
        else
            player.addMessage(Msg_1.Msg.STEP_WRONG);
    }
    /***************************************************************
    Step 4 : return
    ***************************************************************/
    returnCard(playerName, cardId, areaName) {
        const player = this.getPlayerByName(playerName);
        if (this.step === Step_1.Step.RETURN) {
            if (player === this.active) {
                const area = this.getArea(player, areaName);
                const card = player.getCardById(cardId, area);
                if (player.returnCard(card, area)) {
                    this.returnToCommon(card, player);
                    const msg = `${playerName} returns ${card.getName()} from ${areaName}`;
                    this.sendMessage(msg, player);
                }
                else {
                    player.addMessage(Msg_1.Msg.CARD_NOTRETURNABLE);
                }
                if (player.noCardToReturn()) {
                    player.setPassing();
                    this.nextPlayer();
                    //if (this.order.indexOf(this.active) === 0) this.nextStep()
                }
            }
            else
                player.addMessage(Msg_1.Msg.PLAYER_WRONG);
        }
        else
            player.addMessage(Msg_1.Msg.STEP_WRONG);
    }
    returnToCommon(card, from, destroy = false) {
        from.removeProduction(card);
        card.setVisible();
        if (card.type === Card_1.CardType.RESOURCE) {
            this.shared.push(card);
            for (let p of this.players) {
                p.addProduction(card);
            }
        }
        else if (!destroy) {
            this.open.push(card);
        }
    }
    /***************************************************************
    Step 5 : earnings
    ***************************************************************/
    earnings() {
        if (this.step === Step_1.Step.EARNINGS) {
            let maxPrestige = this.players.map(p => p.getPrestigeWithNeigbours()).reduce((a, v) => Math.max(a, v));
            let kingPretenders = [];
            for (let p of this.players) {
                p.earnings();
                if (p.getPrestigeWithNeigbours() === maxPrestige) {
                    // Every prentender earns the max prestige bonus (0 to 3 gold)
                    p.maxPrestigeBonus();
                    kingPretenders.push(p);
                }
                p.unmakeKing();
            }
            const i = Math.floor((kingPretenders.length) * Math.random());
            this.makeNewKing(kingPretenders[i]);
        }
        this.nextStep();
    }
    makeNewKing(king, init = false) {
        king.makeKing();
        this.sendMessage(`${king.getName()} is new king`);
        const left = king.getNeighbour(Area_1.Area.LEFT);
        const right = king.getNeighbour(Area_1.Area.RIGHT);
        // Every neighbour of the king earns 1 gold
        if (!init) {
            left.earGold(1);
            right.earGold(1);
            console.log(`${left.getName()} earns 1 gold and ${right.getName()} earns 1 gold`);
        }
        // Generate game order
        this.order = [];
        this.order.push(king, left, right);
        const kingIndice = this.players.indexOf(king);
        const leftIndice = this.players.indexOf(left);
        const rightIndice = this.players.indexOf(right);
        if (kingIndice === 0) {
            for (let i = 2; i < this.players.length - 1; i++)
                this.order.push(this.players[i]);
        }
        else if (kingIndice === this.players.length - 1) {
            for (let i = 1; i < this.players.length - 2; i++)
                this.order.push(this.players[i]);
        }
        else {
            for (let i = rightIndice + 1; i < this.players.length; i++)
                this.order.push(this.players[i]);
            for (let i = 0; i < leftIndice; i++)
                this.order.push(this.players[i]);
        }
        console.log(`New order : ${this.order.map(v => v.getName())}`);
    }
    /***************************************************************
    Step 6 : Attack
    ***************************************************************/
    beginAttack(attackerName, attackerArmy, defenderName) {
        const player = this.getPlayerByName(attackerName);
        if (this.step === Step_1.Step.ATTACK) {
            if (player === this.active) {
                if (this.attack === null) {
                    console.log(`${attackerName} attacks ${defenderName}`);
                    const defender = this.getPlayerByName(defenderName);
                    this.attack = new Attack_1.Attack(this.order, player, attackerArmy, defender);
                    const sendArmyNext = this.attack.nextPlayer();
                    if (sendArmyNext)
                        this.active = sendArmyNext;
                    else
                        this.endAttack();
                }
                else
                    player.addMessage(Msg_1.Msg.ATTACK_ALREADYBEGUN);
            }
            else
                player.addMessage(Msg_1.Msg.PLAYER_WRONG);
        }
        else
            player.addMessage(Msg_1.Msg.STEP_WRONG);
    }
    doNotAttack(playerName) {
        const player = this.getPlayerByName(playerName);
        if (this.step === Step_1.Step.ATTACK) {
            if (player === this.active) {
                this.active.setPassing();
                this.nextPlayer();
            }
            else
                player.addMessage(Msg_1.Msg.PLAYER_WRONG);
        }
        else
            player.addMessage(Msg_1.Msg.STEP_WRONG);
    }
    supportAttack(playerName, army, supportType) {
        const player = this.getPlayerByName(playerName);
        if (this.step === Step_1.Step.ATTACK) {
            if (player === this.active) {
                if (this.attack != null) {
                    if (this.attack.support(player, supportType, army)) {
                        console.log(`${playerName} participation : ${supportType}`);
                        this.active = this.attack.nextPlayer();
                        if (!this.active)
                            throw "Fatal error";
                    }
                    else
                        player.addMessage(Msg_1.Msg.ATTACK_UNKNOWNSUPPORT);
                }
                else
                    player.addMessage(Msg_1.Msg.ATTACK_NOTBEGUN);
            }
            else
                player.addMessage(Msg_1.Msg.PLAYER_WRONG);
        }
        else
            player.addMessage(Msg_1.Msg.STEP_WRONG);
    }
    defend(defenderName, army) {
        const player = this.getPlayerByName(defenderName);
        if (this.step === Step_1.Step.ATTACK) {
            if (player === this.active) {
                if (this.attack != null) {
                    if (this.attack.defend(player, army)) {
                        console.log(`${defenderName} army : ${army}`);
                        this.active = this.attack.attacker.player;
                    }
                    else
                        player.addMessage(Msg_1.Msg.PLAYER_WRONG);
                }
                else
                    player.addMessage(Msg_1.Msg.ATTACK_NOTBEGUN);
            }
            else
                player.addMessage(Msg_1.Msg.PLAYER_WRONG);
        }
        else
            player.addMessage(Msg_1.Msg.STEP_WRONG);
    }
    endAttack() {
        if (this.step === Step_1.Step.ATTACK) {
            //OLD condition : && (this.attack.resolution <= 0 || this.attack.defender.player.getCards().length === 0)
            this.attack = null;
            this.active.setPassing();
            this.nextPlayer();
        }
    }
    destroyCard(attackerName, defenderName, cardId, areaName) {
        console.log('destroyCard');
        const attacker = this.getPlayerByName(attackerName);
        if (this.step === Step_1.Step.ATTACK) {
            if (attacker === this.active) {
                const defender = this.getPlayerByName(defenderName);
                const area = this.getArea(defender, areaName);
                const card = defender.getCardById(cardId, area);
                //bad code
                if (this.attack.destroyOneCard(attacker, defender, card, area)) {
                    this.returnToCommon(card, defender, true);
                    const msg = `${defenderName} has ${card.getName()} destroyed from ${areaName}`;
                    this.sendMessage(msg, attacker);
                }
                else
                    attacker.addMessage(Msg_1.Msg.ANY);
            }
            else
                attacker.addMessage(Msg_1.Msg.PLAYER_WRONG);
        }
        else
            attacker.addMessage(Msg_1.Msg.STEP_WRONG);
    }
    /***************************************************************
    Next player and next step
    ***************************************************************/
    nextPlayer() {
        let i = this.order.indexOf(this.active);
        let passingPlayers = 0;
        do {
            i++;
            if (i === this.players.length)
                i = 0;
            if (this.order[i].isPassing())
                passingPlayers++;
        } while (this.order[i].isPassing() && passingPlayers < this.order.length);
        if (passingPlayers === this.order.length) {
            this.nextStep();
        }
        else {
            this.active = this.order[i];
        }
    }
    nextStep() {
        for (let player of this.players) {
            player.unsetPassing();
            if (player.isKing())
                this.active = player;
        }
        if (this.step === Step_1.Step.ATTACK) {
            this.step = Step_1.Step.INIT;
            this.turn++;
        }
        else {
            this.step++;
        }
        // To be check when the return phase begins :
        if (this.step === Step_1.Step.RETURN) {
            // If a player has no card to return it is passing
            for (let p of this.players)
                if (p.noCardToReturn())
                    p.setPassing();
            // If king has no card to return go next()
            if (this.active.noCardToReturn())
                this.nextPlayer();
        }
    }
    /***************************************************************
    JSON output
    ***************************************************************/
    jsonReplacer(key, value) {
        if (key === 'leftPlayer' || key === 'rightPlayer') {
            return key.name;
        }
        else {
            return value;
        }
    }
    getJson() {
        const s = JSON.stringify(this, this.jsonReplacer);
        return JSON.parse(s);
    }
    /***************************************************************
    General functions
    ***************************************************************/
    sendMessage(msg, player = null) {
        console.log(msg);
        if (player) {
            player.addMessage(msg);
        }
        else { // broadcast
            for (const p of this.players)
                p.addMessage(msg);
        }
    }
    getArea(player, areaName) {
        if (areaName === `${player.getName()}-left`)
            return Area_1.Area.LEFT;
        else if (areaName === `${player.getName()}-center`)
            return Area_1.Area.CENTER;
        else if (areaName === `${player.getName()}-right`)
            return Area_1.Area.RIGHT;
        else
            return null;
    }
    totalCardsInPlayersHand() {
        return this.players.map(p => p.cardsInHand()).reduce((sum, cards) => sum + cards);
    }
    getPlayerByName(name) {
        for (const player of this.players) {
            if (player.getName() === name)
                return player;
        }
    }
    getPlayersNumber() {
        return this.players.length;
    }
    getCardById(id, area) {
        if (area === Area_1.Area.STOCK) {
            for (const card of this.stock) {
                if (card.getId() === id)
                    return card;
            }
        }
        else if (area === Area_1.Area.OPEN) {
            for (const card of this.open) {
                if (card.getId() === id)
                    return card;
            }
        }
        else
            return null;
    }
    /***************************************************************
    Set initial test situation
    Testing purpose only
    ***************************************************************/
    test(situation) {
        console.log('Testing initial set up');
        this.addCardFromReserveToStock(this.getPlayersNumber() * Game.PARAM_NB_CARDS_DRAWN_BY_TURN * 2);
        if (situation >= Step_1.Step.DRAW) {
            console.log('2 cards for each player, some cards in Open area');
            for (let i = 0; i < 5; i++) {
                const c = this.takeFromStock(this.stock[0]);
                this.open.concat(c);
            }
            for (let p of this.players) {
                for (let i = 0; i < 2; i++) {
                    const c = this.takeFromStock(this.stock[0]);
                    p.addToHand(c);
                }
            }
        }
        if (situation >= Step_1.Step.PLAY) {
            console.log('Each player draw one more card');
            for (let p of this.players) {
                for (let i = 0; i < 1; i++) {
                    const c = this.takeFromStock(this.stock[0]);
                    p.addToHand(c);
                }
            }
        }
        if (situation >= Step_1.Step.ACTIVATE) {
            console.log('Three cards played');
            for (let p of this.players) {
                for (let i = 0; i < 1; i++) {
                    p.playCard(p.getHand()[0], Area_1.Area.CENTER);
                }
                p.playCard(p.getHand()[0], Area_1.Area.LEFT);
                p.playCard(p.getHand()[0], Area_1.Area.RIGHT);
            }
        }
        if (situation >= Step_1.Step.RETURN) {
            console.log('8 cards played');
            for (let p of this.players) {
                for (let i = 0; i < 5; i++) {
                    const c = this.takeFromStock(this.stock[0]);
                    p.addToHand(c);
                }
            }
            for (let p of this.players) {
                for (let i = 0; i < 3; i++) {
                    p.playCard(p.getHand()[0], Area_1.Area.CENTER);
                }
                p.playCard(p.getHand()[0], Area_1.Area.LEFT);
                p.playCard(p.getHand()[0], Area_1.Area.RIGHT);
            }
        }
        if (situation >= Step_1.Step.RETURN) {
            console.log('All card visibles (WARNING : card not really activate in this test mode, neighbours do not earn production)');
            for (let p of this.players) {
                for (let c of p.getCenter()) {
                    c.setVisible();
                    p.addProduction(c);
                }
                for (let c of p.getLeft()) {
                    c.setVisible();
                    p.addProduction(c);
                }
                for (let c of p.getRight()) {
                    c.setVisible();
                    p.addProduction(c);
                }
            }
        }
        if (situation >= Step_1.Step.EARNINGS) {
            this.earnings();
        }
        this.step = situation;
    }
}
exports.Game = Game;
Game.JSONPATH = 'json';
Game.PARAM_NB_CARDS_DRAWN_BY_TURN = 3;
