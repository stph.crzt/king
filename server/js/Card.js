"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const uuid_1 = require("uuid");
var CardType;
(function (CardType) {
    CardType["RESOURCE"] = "resource";
    CardType["ARMY"] = "army";
    CardType["DEFENCE"] = "defence";
    CardType["GOLD"] = "gold";
    CardType["PRESTIGE"] = "prestige";
})(CardType = exports.CardType || (exports.CardType = {}));
class Card {
    constructor(card) {
        Object.assign(this, card);
        //TODO check ref integrity for cost and resource
        this.visible = true;
        this.id = uuid_1.v4();
    }
    getId() {
        return this.id;
    }
    getName() {
        return this.name;
    }
    getResources() {
        return this.resources;
    }
    isInStock() {
        //TODO
        return true;
    }
    isType(type) {
        return this.type === type;
    }
    getOwner() {
        //TODO
    }
    setVisible() {
        this.visible = true;
    }
    setInvisible() {
        this.visible = false;
    }
    isVisible() {
        return this.visible;
    }
}
exports.Card = Card;
