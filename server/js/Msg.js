"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Msg;
(function (Msg) {
    Msg["DONE"] = "I've done what you asked.";
    Msg["STEP_WRONG"] = "Not the right step to do so...";
    Msg["PLAYER_WRONG"] = "Not your turn...";
    Msg["CARD_TOOMANY"] = "Already three cards in hand.";
    Msg["CARD_CANTPLAY"] = "Can't play card here";
    Msg["CARD_NOTACTIVABLE"] = "This card is not activable (not enough resources).";
    Msg["CARD_ALREADYACTIVE"] = "This card is already active";
    Msg["PASSING"] = "You passed";
    Msg["CARD_NOTRETURNABLE"] = "Card can't be returned (max number of card not exceeded in this area) ";
    Msg["ATTACK_ALREADYBEGUN"] = "Attack can't be started, another attack is still in progress";
    Msg["ATTACK_NOTBEGUN"] = "Attack can't be supported, no attack in progress";
    Msg["ATTACK_UNKNOWNSUPPORT"] = "Can't understand what kind of support your trying to do";
    Msg["ANY"] = "Something went wrong";
})(Msg = exports.Msg || (exports.Msg = {}));
