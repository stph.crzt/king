import { Player } from './Player'
import { Card } from './Card'
import { Area } from './Area'


class Engagement {
  player: Player
  army: number
  constructor(player: Player, army: number = null) {
    this.player = player
    this.army = army
  }

  addArmy (army: number) {
    this.army += army
  }

}

export class Attack {
  attacker: Engagement
  attackerSupport: Engagement[] = []
  defender: Engagement
  defenderSupport: Engagement[] = []
  undefinedPlayers: Player[]
  resolution: number

  private spendArmyCost(player: Player, armySent: number): boolean {
    let cost: number
    cost = Math.min(player.getArmy(), armySent)
    console.log(cost)
    if (armySent > player.getArmy()) cost += (armySent - player.getArmy()) * 2
    console.log(cost)
    console.log(player.getGoldStock())
    if (player.getGoldStock() >= cost) {
      player.spendGold(cost)
      return true
    }
    else return false
  }

  constructor(order: Player[], attacker: Player, attackerArmy: number, defender: Player) {
    if (this.spendArmyCost(attacker, attackerArmy)) {
      this.attacker = new Engagement(attacker, attackerArmy)
      this.defender = new Engagement(defender)
      this.undefinedPlayers = [].concat(order)
      this.undefinedPlayers.splice(this.undefinedPlayers.indexOf(attacker), 1)
      this.undefinedPlayers.splice(this.undefinedPlayers.indexOf(defender), 1)
    }
    else throw 'Fatal error'
  }

  public support(player: Player, supportType: string, army:number = 0): boolean {
    if (this.spendArmyCost(player, army)) {
      if (this.undefinedPlayers[0] === player) {
        if (supportType === 'attack') {
          this.attackerSupport.push(new Engagement(player, army))
          this.undefinedPlayers.shift()
          return true
        }
        else if (supportType === 'defence') {
          this.defenderSupport.push(new Engagement(player, army))
          this.undefinedPlayers.shift()
          return true
        }
        else if (supportType === 'pass') {
          this.undefinedPlayers.shift()
          return true
        }
      }
    }
    else throw 'Fatal error'
  }

  public defend(player: Player, army:number):boolean {
    if (this.spendArmyCost(player, army)) {
      if (this.defender.player === player) {
        this.defender.army = army
        this.resolution = this.calculateResolution()
        return true
      }
    }
    else throw 'Fatal error'
  }

  private calculateResolution(): number {
    let resolution: number = this.attacker.army - this.defender.army - this.defender.player.getDefence()
    if (this.attackerSupport.length > 0) resolution += this.attackerSupport.map(v => v.army).reduce((total, v) => total + v)
    if (this.defenderSupport.length > 0) resolution -= this.defenderSupport.map(v => v.army).reduce((total, v) => total + v)
    return resolution
  }

  public nextPlayer(): Player {
    if (this.undefinedPlayers.length > 0) return this.undefinedPlayers[0]
    else if (this.defender.army == null) return this.defender.player
    else return null
  }

  public destroyOneCard (attacker: Player, defender: Player, card: Card, area: Area): boolean {
    if (attacker === this.attacker.player && defender === this.defender.player && this.resolution > 0) {
      this.defender.player.destroyCard(card, area)
      this.resolution--
      return true
    }
  }

/**
  private isOver(): boolean {
    return this.undefinedPlayers.length === 0 && this.defender.army != null
  }
**/

}
