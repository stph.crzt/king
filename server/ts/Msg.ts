export enum Msg {
  DONE = 'I\'ve done what you asked.',
  STEP_WRONG = 'Not the right step to do so...',
  PLAYER_WRONG = 'Not your turn...',
  CARD_TOOMANY = 'Already three cards in hand.',
  CARD_CANTPLAY = 'Can\'t play card here',
  CARD_NOTACTIVABLE = 'This card is not activable (not enough resources).',
  CARD_ALREADYACTIVE = 'This card is already active',
  PASSING = 'You passed',
  CARD_NOTRETURNABLE = 'Card can\'t be returned (max number of card not exceeded in this area) ',
  ATTACK_ALREADYBEGUN = 'Attack can\'t be started, another attack is still in progress',
  ATTACK_NOTBEGUN = 'Attack can\'t be supported, no attack in progress',
  ATTACK_UNKNOWNSUPPORT = 'Can\'t understand what kind of support your trying to do',
  ANY = 'Something went wrong'
}
