import { v4 as uuidv4 } from 'uuid'
import { Card } from './Card'
import { Area, AreaMaxCards } from './Area'

export class Player {
  private name: string
  private password: string
  private leftPlayer: Player
  private rightPlayer: Player
  private hand: Card[] = []
  private center: Card[] = []
  private left: Card[] = []
  private right: Card[] = []
  private resources: string[] = []
  private army: number = 0
  private defence: number = 0
  private gold: number = 0
  private goldStock: number = 0
  private prestige: number  = 0
  private prestigeStock: number = 0
  private king: boolean = false
  private passing: boolean = false //if true player is passing for this step
  private messages: string[] = []
  static MAX_MESSAGES: number = 10

  constructor (player: JSON, ) {
    Object.assign(this, player)
  }
  getName(): string {
    return this.name
  }
  getPrestige(): number {
    return this.prestige
  }
  getGoldStock(): number {
    return this.goldStock
  }
  getDefence():number {
    return this.defence
  }
  getArmy():number {
    return this.army
  }
  getPrestigeWithNeigbours(): number {
    return this.prestige + (this.leftPlayer.prestige + this.rightPlayer.prestige) / 1000
  }
  public getCards() {
    return this.hand.concat(this.left).concat(this.center).concat(this.right)
  }
  public maxPrestigeBonus() {
    if (this.prestige < 3) this.goldStock += this.prestige
    else this.goldStock += 3
    console.log(`${this.getName()} earns prestige bonus`)
  }
  public makeKing() {
    this.king = true
  }
  public unmakeKing() {
    this.king = false
  }
  public isKing(): boolean {
    return this.king
  }
  public earGold(gold: number) {
    this.goldStock += gold
  }
  public spendGold(gold: number) {
    this.goldStock -= gold
  }
  setNeighbours (leftPlayer: Player, rightPlayer: Player) {
    this.leftPlayer = leftPlayer
    this.rightPlayer = rightPlayer
  }
  getNeighbour (area: Area) {
    if (area === Area.LEFT) {
      return this.leftPlayer
    }
    else if (area === Area.RIGHT) {
      return this.rightPlayer
    }
    else {
      return null
    }
  }
  public addMessage (message: string) {
    this.messages.unshift(message)
    if (this.messages.length > Player.MAX_MESSAGES) this.messages.pop()
  }

  public noCardToReturn(): boolean {
    return this.getLeft().length <= AreaMaxCards.LEFT
        && this.getRight().length <= AreaMaxCards.RIGHT
        && this.getCenter().length <= AreaMaxCards.CENTER
  }

  public getCardById(cardId: string, area: Area = null): Card {
    if (area === Area.HAND) {
      for (const card of this.hand) {
        if (card.getId() === cardId) return card
      }
    }
    if (area === Area.LEFT) {
      for (const card of this.left) {
        if (card.getId() === cardId) return card
      }
    }
    if (area === Area.CENTER) {
      for (const card of this.center) {
        if (card.getId() === cardId) return card
      }
    }
    if (area === Area.RIGHT) {
      for (const card of this.right) {
        if (card.getId() === cardId) return card
      }
    }
    // search any area
    if (area === null) {
      for (const card of this.getCards()) {
        if (card.getId() === cardId) return card
      }
    }
    // else
    return null
  }

  /** In order to be activable the cost of the card must be already available for the player **/
  public isActivable (card: Card) {
    let available: string[] = this.resources.map((v) => v)
    if (card.cost) {
      for (const resource of card.cost) {
        const i: number = available.indexOf(resource)
        if (i >= 0) {
          available.splice(i,1)
        }
        else {
          return false
        }
      }
    }
    return true
  }


  getHand(): Card[] {
    return this.hand
  }
  getCenter(): Card[] {
    return this.center
  }
  getLeft(): Card[] {
    return this.left
  }
  getRight(): Card[] {
    return this.right
  }

  addToHand(card: Card) {
    this.hand.push(card)
  }

  private takeFromHand(card: Card) {
    this.hand.splice(this.hand.indexOf(card), 1)
  }

  public addProduction (card: Card) {
    if (card.army) this.army += card.army
    if (card.defence) this.defence += card.defence
    if (card.prestige) this.prestige += card.prestige
    if (card.gold) this.gold += card.gold
    if (card.resources) this.resources = this.resources.concat(card.getResources())
  }

  public removeProduction (card: Card) {
    if (card.isVisible()) {
      if (card.army) this.army -= card.army
      if (card.defence) this.defence -= card.defence
      if (card.prestige) this.prestige -= card.prestige
      if (card.gold) this.gold -= card.gold
      if (card.resources) for (let r of card.resources) this.resources.splice(this.resources.indexOf(r), 1)
    }
  }

  public playCard(card: Card, area: Area): boolean {
    if (this.hand.includes(card)) {
      //testing possibility

      if (area === Area.CENTER && this.center.length < AreaMaxCards.CENTER + 1) {
        this.takeFromHand(card)
        this.center.push(card)
        return true
      }
      else if (area === Area.LEFT && this.left.length < AreaMaxCards.LEFT + 1) {
        this.takeFromHand(card)
        this.left.push(card)
        return true
      }
      else if (area === Area.RIGHT && this.right.length < AreaMaxCards.RIGHT +1 ) {
        this.takeFromHand(card)
        this.right.push(card)
        return true
      }
      else {
        // error
        return false
      }
    }
  }

  public returnCard(card: Card, area: Area) {
    if (area === Area.CENTER && this.center.length > AreaMaxCards.CENTER && this.center.includes(card)) {
      this.center.splice(this.center.indexOf(card), 1)
      return true
    }
    else if (area === Area.LEFT && this.left.length > AreaMaxCards.LEFT && this.left.includes(card)) {
      this.left.splice(this.left.indexOf(card), 1)
      return true
    }
    else if (area === Area.RIGHT && this.right.length > AreaMaxCards.RIGHT && this.right.includes(card)) {
      this.right.splice(this.right.indexOf(card), 1)
      return true
    }
  }

  public destroyCard(card: Card, area: Area) {
    if (area === Area.CENTER && this.center.includes(card)) {
      this.center.splice(this.center.indexOf(card), 1)
      return true
    }
    else if (area === Area.LEFT && this.left.includes(card)) {
      this.left.splice(this.left.indexOf(card), 1)
      return true
    }
    else if (area === Area.RIGHT && this.right.includes(card)) {
      this.right.splice(this.right.indexOf(card), 1)
      return true
    }
  }

  public earnings() {
    this.goldStock += this.gold
    this.prestigeStock += this.prestige
  }

  public setPassing() {
    this.passing = true
  }
  public unsetPassing() {
    this.passing = false
  }
  public isPassing() {
    return this.passing
  }

  cardsInHand(): number {
    return this.hand.length
  }

}
