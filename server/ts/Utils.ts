export class Utils {
  static shuffle(array: any[]): any[] {
    const rList = array.map((v) => [Math.random(), v])
    return [].concat(rList.sort().map((v) => v[1]))
  }
}
