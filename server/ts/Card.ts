import { v4 as uuidv4 } from 'uuid'

export enum CardType {
  RESOURCE = 'resource',
  ARMY = 'army',
  DEFENCE = 'defence',
  GOLD = 'gold',
  PRESTIGE = 'prestige'
}

export class Card {

  private id: string
  private name: string
  public cost: string[]  // list of resources names
  public type: CardType
  public resources: string[] // list of resources names
  public army: number
  public defence: number
  public gold: number
  public prestige: number
  private visible: boolean

  constructor (card: JSON) {
    Object.assign(this, card)
    //TODO check ref integrity for cost and resource
    this.visible = true
    this.id = uuidv4()
  }

  public getId(): string {
    return this.id
  }

  getName() {
    return this.name
  }

  getResources(): string[] {
      return this.resources
  }

  isInStock(): boolean {
    //TODO
    return true
  }

  isType(type: CardType): boolean {
    return this.type === type
  }

  getOwner () {
    //TODO
  }

  setVisible() {
    this.visible = true
  }
  setInvisible() {
    this.visible = false
  }
  public isVisible() {
    return this.visible
  }
}
