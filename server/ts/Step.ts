export enum Step {
  INIT,
  DRAW,
  PLAY,
  ACTIVATE,
  RETURN,
  EARNINGS,  
  ATTACK
}
