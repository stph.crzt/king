#!/usr/bin/env node

/** Setting up express server **/
import express from 'express'
const app = express()
app.use(express.urlencoded({ extended: true }))
app.use(express.json())
app.use(express.static('client/css'))
app.use(express.static('client/js'))


/** Arguments management
  --port change port from 8081 to any
**/
let port: Number = 8082
process.argv.forEach((val, index) => {
  if (val === '--port' || val === '-p') {
    let p: Number = parseInt(process.argv[index + 1])
    if (p) {
      port = p
      console.log(`Port : ${port}`)
    }
    else {
      console.error(`Error of port specification (--port) ; reset to default port ${port}`)
    }
  }
})

/** Proper libraries **/
import { Game } from './Game'

const g = new Game()
g.init()
console.log(g)

app.get('/', async (request, result) => {
  result.render('index.ejs')
})

app.get('/init', async (request, result) => {
  console.log('reloading...')
  //TODO check game state (if other players have acted)
  result.json(g.getJson())
})

app.post('/order', async (request, result) => {
  console.log(request.body)
  if (request.body.order === 'refill') {
    g.refill()
  }
  else if (request.body.order === 'takeCard') {
    if (request.body.area === 0) {
      g.takeCardFromStock(request.body.player, request.body.card)
    }
    else if (request.body.area === 1) {
      g.takeCardFromOpen(request.body.player, request.body.card)
    }
  }
  else if (request.body.order === 'playCard') {
    g.playCardFromHand(request.body.player, request.body.card, request.body.area)
  }
  else if (request.body.order === 'activateCard') {
    g.activateCard(request.body.player, request.body.card, request.body.area)
  }
  else if (request.body.order === 'pass') {
    g.pass(request.body.player)
  }
  else if (request.body.order === 'returnCard') {
    g.returnCard(request.body.player, request.body.card, request.body.area)
  }
  else if (request.body.order === 'earnings') {
    g.earnings()
  }
  else if (request.body.order === 'attack') {
    g.beginAttack(request.body.player, request.body.army, request.body.target)
  }
  else if (request.body.order === 'doNotAttack') {
    g.doNotAttack(request.body.player)
  }
  else if (request.body.order === 'support') {
    g.supportAttack(request.body.player, request.body.army, request.body.support)
  }
  else if (request.body.order === 'defend') {
    g.defend(request.body.player, request.body.army)
  }
  else if (request.body.order === 'endAttack') {
    g.endAttack()
  }
  else if (request.body.order === 'destroyCard') {
    g.destroyCard(request.body.attacker, request.body.defender, request.body.card, request.body.area)
  }

  //TODO check game state after player action
  result.json(g.getJson())
})

app.listen(port)
