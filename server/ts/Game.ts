import { Utils } from './Utils'
import { Player } from './Player'
import { Card, CardType } from './Card'
import { Area, AreaMaxCards } from './Area'
import { Step } from './Step'
import { Msg } from './Msg'
import { Attack } from './Attack'

// fs
import fs from 'fs-extra'

export class Game {

  static JSONPATH = 'json'
  static PARAM_NB_CARDS_DRAWN_BY_TURN = 3
  private name: string
  private players: Player[] = []
  private reserve: Card[] = []
  private stock: Card[] = []
  private open: Card[] = []
  private shared: Card[] = []
  private turn: number = 1
  private step: Step = Step.INIT
  private order: Player[] = []
  private active: Player
  private attack : Attack = null

  constructor () {
  }

  async init() {

    //Init cards
    const gameJson = await fs.readJson(`${Game.JSONPATH}/games/basic.json`)
    this.name = gameJson.name
    for (const set of gameJson.sets) {
      const setJson = await fs.readJson(`${Game.JSONPATH}/sets/${set}.json`)
      for (const card of setJson.cards) {
        const cardJson = await fs.readJson(`${Game.JSONPATH}/cards/${card}.json`)
        this.reserve.push(new Card(cardJson))
      }
    }

    // Init players
    for (const player of gameJson.players) {
      const playerJson = await fs.readJson(`${Game.JSONPATH}/players/${player}.json`)
      this.players.push(new Player(playerJson))
    }
    this.players = Utils.shuffle(this.players)

    let leftPlayerIndice: number = this.players.length - 1 // left player of first player is last player
    let rightPlayerIndice: number = 1 // right player of first player (i.e. player n°0) is player n°1
    for (let i = 0; i < this.players.length; i++) {
      this.players[i].setNeighbours(this.players[leftPlayerIndice], this.players[rightPlayerIndice])
      leftPlayerIndice++
      rightPlayerIndice++
      if (rightPlayerIndice === this.players.length) {
        rightPlayerIndice = 0
      }
      if (leftPlayerIndice === this.players.length) {
        leftPlayerIndice = 0
      }
    }

    this.active = this.players[0]
    const i = Math.floor((this.players.length) * Math.random())
    this.makeNewKing(this.players[i], true)


    this.test(Step.ACTIVATE)

  }





  /***************************************************************
  Step 0 : Init
  ***************************************************************/
  public refill() {
    //this.stock = Utils.shuffle(this.stock)
    //this.nextStep()
    if (this.step === Step.INIT) {
      this.addCardFromReserveToStock(this.getPlayersNumber() * Game.PARAM_NB_CARDS_DRAWN_BY_TURN)
      if (this.turn === 1) this.addCardFromReserveToStock(this.getPlayersNumber() * Game.PARAM_NB_CARDS_DRAWN_BY_TURN)
      this.nextStep()
    }
  }

  private addCardFromReserveToStock(n: number) {
    // TODO management if no more cards in reserve
    let c: Card
    for (let i = 0; i < n; i++) {
      c = this.reserve.shift()
      c.setInvisible()
      this.stock.push(c)
    }
    Utils.shuffle(this.stock)
  }

  /***************************************************************
  Step 1 : Draw
  ***************************************************************/
  public takeCardFromStock(playerName: string, cardName: string) {
    this.takeCardFrom(playerName, cardName, Area.STOCK)
  }
  public takeCardFromOpen(playerName: string, cardName: string) {
    this.takeCardFrom(playerName, cardName, Area.OPEN)
  }

  private takeCardFrom(playerName: string, cardId: string, area: Area) {
    const player: Player = this.getPlayerByName(playerName)
    if (this.step === Step.DRAW) {
      if (player === this.active) {
        const card: Card = this.getCardById(cardId, area)
        if (player.cardsInHand() < 3 && card) {
          if (area === Area.STOCK) {
            this.takeFromStock(card)
          }
          else if (area === Area.OPEN) {
            this.takeFromOpen(card)
            card.setInvisible()
          }
          player.addToHand(card)
          this.nextPlayer()
          this.sendMessage(`${playerName} takes ${card.getName()} from ${area}`, player)
          if (this.totalCardsInPlayersHand() === this.players.length * 3) {
            this.nextStep()
          }
        }
        else player.addMessage(Msg.CARD_TOOMANY)
      }
      else player.addMessage(Msg.PLAYER_WRONG)
    }
    else player.addMessage(Msg.STEP_WRONG)
  }
  private takeFromStock(card: Card): Card {
    return this.stock.splice(this.stock.indexOf(card), 1)[0]
  }
  private takeFromOpen(card: Card): Card {
    return this.open.splice(this.open.indexOf(card), 1)[0]
  }

  /***************************************************************
  Step 2 : Play
  ***************************************************************/
  public playCardFromHand(playerName: string, cardId: string, areaName: string)   {
    const player: Player = this.getPlayerByName(playerName)
    if (this.step === Step.PLAY) {
      if (player === this.active) {
        const area = this.getArea(player, areaName)
        const card: Card = player.getCardById(cardId, Area.HAND)
        if (player.playCard(card, area)) {
          this.nextPlayer()
          if (this.totalCardsInPlayersHand() === 0) {
            this.nextStep()
            player.addMessage(`${playerName} plays ${card.getName()} to ${areaName}`)
          }
        }
        else player.addMessage(Msg.CARD_CANTPLAY)
      }
      else player.addMessage(Msg.PLAYER_WRONG)
    }
    else player.addMessage(Msg.STEP_WRONG)
  }

  /***************************************************************
  Step 3 : Activate
  ***************************************************************/
  public activateCard(playerName: string, cardId: string, areaName: string) {
    const player: Player = this.getPlayerByName(playerName)
    if (this.step === Step.ACTIVATE) {
      if (player === this.active) {
        const area = this.getArea(player, areaName)
        const card: Card = player.getCardById(cardId, area)
        // Card already activated ?
        if (card.isVisible()) {
          player.addMessage(Msg.CARD_ALREADYACTIVE)
          return false
        }
        // Enough resources to activate ?
        else if (player.isActivable(card)) {
          card.setVisible()
          player.addProduction(card)
          if (area === Area.LEFT || area === Area.RIGHT) {
            player.getNeighbour(area).addProduction(card)
          }
          this.nextPlayer()
          player.addMessage(Msg.DONE)
        }
        else player.addMessage(Msg.CARD_NOTACTIVABLE)
      }
      else player.addMessage(Msg.PLAYER_WRONG)
    }
    else player.addMessage(Msg.STEP_WRONG)
  }

  public pass (playerName: string) {
    const player: Player = this.getPlayerByName(playerName)
    if (this.step === Step.ACTIVATE) {
      if (player === this.active) {
        player.setPassing()
        this.nextPlayer()
        player.addMessage(Msg.PASSING)
      }
      else player.addMessage(Msg.PLAYER_WRONG)
    }
    else player.addMessage(Msg.STEP_WRONG)
  }

  /***************************************************************
  Step 4 : return
  ***************************************************************/
  public returnCard (playerName: string, cardId: string, areaName: string) {
    const player = this.getPlayerByName(playerName)
    if (this.step === Step.RETURN) {
      if (player === this.active) {
        const area = this.getArea(player, areaName)
        const card = player.getCardById(cardId, area)
        if (player.returnCard(card, area)) {
          this.returnToCommon(card, player)
          const msg: string = `${playerName} returns ${card.getName()} from ${areaName}`
          this.sendMessage(msg, player)
        }
        else {
          player.addMessage(Msg.CARD_NOTRETURNABLE)
        }
        if (player.noCardToReturn()) {
          player.setPassing()
          this.nextPlayer()
          //if (this.order.indexOf(this.active) === 0) this.nextStep()
        }
      }
      else player.addMessage(Msg.PLAYER_WRONG)
    }
    else player.addMessage(Msg.STEP_WRONG)
  }

  private returnToCommon(card: Card, from: Player, destroy = false) {
    from.removeProduction(card)
    card.setVisible()
    if (card.type === CardType.RESOURCE) {
      this.shared.push(card)
      for (let p of this.players) {
        p.addProduction(card)
      }
    }
    else if (! destroy){
      this.open.push(card)
    }
  }

  /***************************************************************
  Step 5 : earnings
  ***************************************************************/
  public earnings() {
    if (this.step === Step.EARNINGS) {
      let maxPrestige:number = this.players.map(p => p.getPrestigeWithNeigbours()).reduce((a, v) => Math.max(a, v))
      let kingPretenders:Player[] = []
      for (let p of this.players) {
        p.earnings()
        if (p.getPrestigeWithNeigbours() === maxPrestige) {
          // Every prentender earns the max prestige bonus (0 to 3 gold)
          p.maxPrestigeBonus()
          kingPretenders.push(p)
        }
        p.unmakeKing()
      }
      const i = Math.floor((kingPretenders.length) * Math.random())
      this.makeNewKing(kingPretenders[i])
    }
    this.nextStep()
  }

  private makeNewKing(king: Player, init: boolean = false) {

    king.makeKing()
    this.sendMessage(`${king.getName()} is new king`)

    const left: Player = king.getNeighbour(Area.LEFT)
    const right: Player = king.getNeighbour(Area.RIGHT)
    // Every neighbour of the king earns 1 gold
    if (!init) {
      left.earGold(1)
      right.earGold(1)
      console.log(`${left.getName()} earns 1 gold and ${right.getName()} earns 1 gold`)
    }
    // Generate game order
    this.order = []
    this.order.push(king, left, right)
    const kingIndice: number = this.players.indexOf(king)
    const leftIndice: number = this.players.indexOf(left)
    const rightIndice: number = this.players.indexOf(right)
    if (kingIndice === 0) {
      for (let i = 2; i < this.players.length - 1; i++) this.order.push(this.players[i])
    }
    else if (kingIndice === this.players.length - 1) {
      for (let i = 1; i < this.players.length - 2; i++) this.order.push(this.players[i])
    }
    else {
      for (let i = rightIndice + 1; i < this.players.length; i++) this.order.push(this.players[i])
      for (let i = 0; i < leftIndice; i++) this.order.push(this.players[i])
    }
    console.log(`New order : ${this.order.map(v => v.getName())}`)
  }

  /***************************************************************
  Step 6 : Attack
  ***************************************************************/
  public beginAttack (attackerName: string, attackerArmy: number, defenderName: string) {
    const player = this.getPlayerByName(attackerName)
    if (this.step === Step.ATTACK) {
      if (player === this.active) {
        if (this.attack === null) {
          console.log(`${attackerName} attacks ${defenderName}`)
          const defender = this.getPlayerByName(defenderName)
          this.attack = new Attack(this.order, player, attackerArmy, defender)
          const sendArmyNext = this.attack.nextPlayer()
          if (sendArmyNext) this.active = sendArmyNext
          else this.endAttack()
        }
        else player.addMessage(Msg.ATTACK_ALREADYBEGUN)
      }
      else player.addMessage(Msg.PLAYER_WRONG)
    }
    else player.addMessage(Msg.STEP_WRONG)
  }

  public doNotAttack (playerName: string) {
    const player = this.getPlayerByName(playerName)
    if (this.step === Step.ATTACK) {
      if (player === this.active) {
        this.active.setPassing()
        this.nextPlayer()
      }
      else player.addMessage(Msg.PLAYER_WRONG)
    }
    else player.addMessage(Msg.STEP_WRONG)
  }

  public supportAttack (playerName: string, army: number, supportType: string) {
    const player = this.getPlayerByName(playerName)
    if (this.step === Step.ATTACK) {
      if (player === this.active) {
        if (this.attack != null) {
          if (this.attack.support(player, supportType, army)) {
            console.log(`${playerName} participation : ${supportType}`)
            this.active = this.attack.nextPlayer()
            if (! this.active) throw "Fatal error";
          }
          else player.addMessage(Msg.ATTACK_UNKNOWNSUPPORT)
        }
        else player.addMessage(Msg.ATTACK_NOTBEGUN)
      }
      else player.addMessage(Msg.PLAYER_WRONG)
    }
    else player.addMessage(Msg.STEP_WRONG)
  }

  public defend (defenderName: string, army: number) {
    const player = this.getPlayerByName(defenderName)
    if (this.step === Step.ATTACK) {
      if (player === this.active) {
        if (this.attack != null) {
          if (this.attack.defend(player, army)) {
            console.log(`${defenderName} army : ${army}`)
            this.active = this.attack.attacker.player
          }
          else player.addMessage(Msg.PLAYER_WRONG)
        }
        else player.addMessage(Msg.ATTACK_NOTBEGUN)
      }
      else player.addMessage(Msg.PLAYER_WRONG)
    }
    else player.addMessage(Msg.STEP_WRONG)
  }

  public endAttack() {
    if (this.step === Step.ATTACK) {
      //OLD condition : && (this.attack.resolution <= 0 || this.attack.defender.player.getCards().length === 0)
      this.attack = null
      this.active.setPassing()
      this.nextPlayer()
    }
  }

  public destroyCard (attackerName: string, defenderName: string, cardId: string, areaName: string) {
    console.log('destroyCard')
    const attacker = this.getPlayerByName(attackerName)
    if (this.step === Step.ATTACK) {
      if (attacker === this.active) {
        const defender = this.getPlayerByName(defenderName)
        const area = this.getArea(defender, areaName)
        const card = defender.getCardById(cardId, area)
        //bad code
        if (this.attack.destroyOneCard(attacker, defender, card, area)) {
          this.returnToCommon(card, defender, true)
          const msg: string = `${defenderName} has ${card.getName()} destroyed from ${areaName}`
          this.sendMessage(msg, attacker)
        }
        else attacker.addMessage(Msg.ANY)
      }
      else attacker.addMessage(Msg.PLAYER_WRONG)
    }
    else attacker.addMessage(Msg.STEP_WRONG)
  }

  /***************************************************************
  Next player and next step
  ***************************************************************/
    private nextPlayer() {
      let i = this.order.indexOf(this.active)
      let passingPlayers = 0
      do {
        i++
        if (i === this.players.length) i = 0
        if (this.order[i].isPassing()) passingPlayers++
      }
      while(this.order[i].isPassing() && passingPlayers < this.order.length)
      if (passingPlayers === this.order.length) {
        this.nextStep()
      }
      else {
        this.active = this.order[i]
      }
    }

    private nextStep() {
      for (let player of this.players) {
        player.unsetPassing()
        if (player.isKing()) this.active = player
      }
      if (this.step === Step.ATTACK) {
        this.step = Step.INIT
        this.turn++
      }
      else {
        this.step++
      }
      // To be check when the return phase begins :
      if (this.step === Step.RETURN) {
        // If a player has no card to return it is passing
        for (let p of this.players) if (p.noCardToReturn()) p.setPassing()
        // If king has no card to return go next()
        if (this.active.noCardToReturn()) this.nextPlayer()
      }
    }

  /***************************************************************
  JSON output
  ***************************************************************/
  private jsonReplacer(key, value) {
    if (key === 'leftPlayer' || key === 'rightPlayer') {
      return key.name
    }
    else {
      return value
    }
  }

  public getJson(): JSON {
    const s: string = JSON.stringify(this, this.jsonReplacer)
    return JSON.parse(s)
  }

  /***************************************************************
  General functions
  ***************************************************************/
  private sendMessage (msg: string, player: Player = null) {
    console.log(msg)
    if (player) {
      player.addMessage(msg)
    }
    else { // broadcast
      for (const p of this.players) p.addMessage(msg)
    }
  }

  private getArea (player:Player, areaName: string): Area {
    if (areaName === `${player.getName()}-left`) return Area.LEFT
    else if (areaName === `${player.getName()}-center`) return Area.CENTER
    else if (areaName === `${player.getName()}-right`) return Area.RIGHT
    else return null
  }

  private totalCardsInPlayersHand() {
    return this.players.map(p => p.cardsInHand()).reduce((sum, cards) => sum + cards)
  }

  private getPlayerByName(name: string): Player {
    for (const player of this.players) {
      if (player.getName() === name) return player
    }
  }

  private getPlayersNumber(): number {
    return this.players.length
  }


  private getCardById(id: string, area: Area): Card {
    if (area === Area.STOCK) {
      for (const card of this.stock) {
        if (card.getId() === id) return card
      }
    }
    else if (area === Area.OPEN) {
      for (const card of this.open) {
        if (card.getId() === id) return card
      }
    }
    else return null
  }

  /***************************************************************
  Set initial test situation
  Testing purpose only
  ***************************************************************/
  private test(situation: number) {
    console.log('Testing initial set up')

    this.addCardFromReserveToStock(this.getPlayersNumber() * Game.PARAM_NB_CARDS_DRAWN_BY_TURN * 2)

    if (situation >= Step.DRAW) {
      console.log('2 cards for each player, some cards in Open area')
      for (let i = 0; i < 5; i++) {
        const c = this.takeFromStock(this.stock[0])
        this.open.concat(c)
      }
      for (let p of this.players) {
        for (let i = 0; i < 2; i++) {
          const c = this.takeFromStock(this.stock[0])
          p.addToHand(c)
        }
      }
    }
    if (situation >= Step.PLAY) {
      console.log('Each player draw one more card')
      for (let p of this.players) {
        for (let i = 0; i < 1; i++) {
          const c = this.takeFromStock(this.stock[0])
          p.addToHand(c)
        }
      }
    }
    if (situation >= Step.ACTIVATE) {
      console.log('Three cards played')
      for (let p of this.players) {
        for (let i = 0; i < 1; i++) {
          p.playCard(p.getHand()[0], Area.CENTER)
        }
        p.playCard(p.getHand()[0], Area.LEFT)
        p.playCard(p.getHand()[0], Area.RIGHT)
      }
    }
    if (situation >= Step.RETURN) {
      console.log('8 cards played')
      for (let p of this.players) {
        for (let i = 0; i < 5; i++) {
          const c = this.takeFromStock(this.stock[0])
          p.addToHand(c)
        }
      }
      for (let p of this.players) {
        for (let i = 0; i < 3; i++) {
          p.playCard(p.getHand()[0], Area.CENTER)
        }
        p.playCard(p.getHand()[0], Area.LEFT)
        p.playCard(p.getHand()[0], Area.RIGHT)
      }
    }
    if (situation >= Step.RETURN) {
      console.log('All card visibles (WARNING : card not really activate in this test mode, neighbours do not earn production)')
      for (let p of this.players) {
        for (let c of p.getCenter()) {
          c.setVisible()
          p.addProduction(c)
        }
        for (let c of p.getLeft()) {
          c.setVisible()
          p.addProduction(c)
        }
        for (let c of p.getRight()) {
          c.setVisible()
          p.addProduction(c)
        }
      }
    }
    if (situation >= Step.EARNINGS) {
      this.earnings()
    }
    this.step = situation

  }

}
