export enum Area {
  STOCK,
  OPEN,
  SHARED,
  HAND,
  CENTER,
  LEFT,
  RIGHT
}

export enum AreaMaxCards {
  HAND = 3,
  CENTER = 3,
  LEFT = 1,
  RIGHT = 1
}
