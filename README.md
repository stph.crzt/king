npm install typescript
npm install express express-fileupload ejs fs-extra winston uuid
npm install @types/node @types/express @types/express-fileupload @types/fs-extra @types/winston @types/uuid

# TODO current

- TODO Add reserve and init phase : refill stock with reserve


# CHECK
- Suppress confirm of attack orders (slow down the game)

# TODO
- Display cards in reserve
- DETAIL Naming step client side
- ENHANCE set auto-passing at step 3 when no card are activable (and for other steps)

# TODO later
- manage multiple players security
- CHECK RULE Attack : auto-destroy neighbors cards

# Notes
- NOTE : unactive cards can be destroyed
